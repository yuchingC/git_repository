# [AI SW] w2 create micro service with FastAPI & Virtualization
本篇README.md內容為第二週的學習內容，包含FastAPI與Docker的學習。
## 目錄
[TOC]
## 學習心得
## [AI SW] w2-1 create micro service with FastAPI
### 虛擬環境設定
- 使用virtualenv，因為要安裝套件，開啟cmd時需要以系統管理員身分執行
```bash!
#確認python是否安裝
python --version

#確認pip是否安裝
pip --version

#確認pip套件
pip list

#下載virtualenv套件
pip install virtualenv

#建立虛擬環境(在想要創建環境的路徑下，通常建立在專案資料夾下)
virtualenv [環境名稱]

#啟用虛擬環境(windows語法)
cd [專案名稱]
[環境名稱]\Scripts\activate
#或是
cd [虛擬環境所在路徑]
.\activate
#在git bash下(linux語法)
cd [虛擬環境所在路徑]
source Scripts/activate

#退出虛擬環境
deactivate
```
- 在啟用虛擬環境時遇到以下錯誤，用系統管理員身分執行後輸入```Set-ExecutionPolicy RemoteSigned -Scope CurrentUser```開啟權限

![alt text](README_image/image.png)
### fastAPI設定
```bash!
#開啟虛擬環境

#安裝fastapi
pip install fastapi

#開啟fastapi
#方法1: 直接執行python
python main.py
#方法1的另一種方式: 使用fastapi的命令
fastapi dev main.py
#方法2: 使用uvicorn啟動，--reload是指在修改程式碼時自動重新加載
uvicorn app.main:app --reload
#顯示更多詳細訊息
uvicorn main:app --reload --log-level debug
```
### FastAPI常用操作
### FastAPI實作image CRUD
[FastAPI實作image CRUD(main.py)](w2-1/main.py)
## [AI SW] w2-2 Virtualization 
### Docker安裝
- [下載Docker Desktop](https://docs.docker.com/get-docker/)
- 照說明安裝，安裝後需要重新啟動
- 以系統管理員執行cmd
```bash!
#查看版本、是否安裝成功
docker --version
#查看能否運行
docker run hello-world
```
- 在執行```docker run hello-world```之後，Docker Desktop裡的container會出現新的container：
![alt text](README_image/image-1.png)
![alt text](README_image/image-2.png)