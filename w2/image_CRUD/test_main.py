import os
import sys
import pytest
from fastapi.testclient import TestClient
from main import app, UPLOAD_DIRECTORY
from io import BytesIO
from fastapi import FastAPI,HTTPException, UploadFile
from unittest.mock import patch, MagicMock
import subprocess

client = TestClient(app)

@pytest.fixture(scope="module", autouse=True)
def setup_and_teardown():
    # 測試前確保目錄存在
    if not os.path.exists(UPLOAD_DIRECTORY):
        os.makedirs(UPLOAD_DIRECTORY)
    else:
        # 清空目錄
        for file in os.listdir(UPLOAD_DIRECTORY):
            file_path = os.path.join(UPLOAD_DIRECTORY, file)
            if os.path.isfile(file_path):
                os.unlink(file_path)
    yield
    # 測試結束後清理目錄
    for file in os.listdir(UPLOAD_DIRECTORY):
        file_path = os.path.join(UPLOAD_DIRECTORY, file)
        if os.path.isfile(file_path):
            os.unlink(file_path)
    os.rmdir(UPLOAD_DIRECTORY)

def test_read_root():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World"}

def test_upload_image():
    file_name = "test_image.jpg"
    with open(file_name, "wb") as f:
        f.write(os.urandom(1024))  # 建立一個隨機檔案
    with open(file_name, "rb") as f:
        response = client.post("/upload/", files={"file": f})
    assert response.status_code == 200
    assert response.json() == {"filename": file_name}
    os.remove(file_name)

def test_upload_exception(monkeypatch):
    def mock_write(data):
        raise Exception("Mock Exception")

    class MockFile:
        def read(self):
            return b"fake data"
    
    class MockUploadFile:
        filename = "test.jpg"
        file = MockFile()
    
    monkeypatch.setattr("builtins.open", lambda *args, **kwargs: MockFile())
    
    response = client.post("/upload/", files={"file": ("filename", b"fake data", "image/jpeg")})
    assert response.status_code == 500
    assert response.json() == {"detail": "Internal Server Error"}

def test_list_images_success():
    # 創建測試文件
    test_filename = "test_image.jpg"
    with open(os.path.join(UPLOAD_DIRECTORY, test_filename), "w") as f:
        f.write("test")

    response = client.get("/images/")
    assert response.status_code == 200
    assert test_filename in response.json()

def test_list_images_directory_not_found(monkeypatch):
    # 模擬目錄不存在的情況
    def mock_os_listdir(path):
        raise FileNotFoundError("Directory not found")
    
    monkeypatch.setattr(os, "listdir", mock_os_listdir)
    
    response = client.get("/images/")
    assert response.status_code == 500
    assert response.json() == {"detail": "Internal Server Error"}

def test_list_images_permission_error(monkeypatch):
    # 模擬目錄無權訪問的情況
    def mock_os_listdir(path):
        raise PermissionError("Permission denied")
    
    monkeypatch.setattr(os, "listdir", mock_os_listdir)
    
    response = client.get("/images/")
    assert response.status_code == 500
    assert response.json() == {"detail": "Internal Server Error"}

def test_get_image_success():
    # 創建測試文件
    test_filename = "test_image.jpg"
    file_path = os.path.join(UPLOAD_DIRECTORY, test_filename)
    with open(file_path, "w") as f:
        f.write("test")

    response = client.get(f"/images/{test_filename}")
    assert response.status_code == 200
    assert response.headers["content-type"] == "image/jpeg"
    assert response.content == b"test"

def test_get_image_not_found():
    response = client.get("/images/non_existent_image.jpg")
    assert response.status_code == 404
    assert response.json() == {"detail": "Image not found"}

def test_get_image_internal_error(monkeypatch):
    def mock_os_path_exists(path):
        raise Exception("Unexpected error")
    
    monkeypatch.setattr(os.path, "exists", mock_os_path_exists)

    response = client.get("/images/test_image.jpg")
    assert response.status_code == 500
    assert response.json() == {"detail": "Internal Server Error"}

def test_update_image_success():
    # 創建測試文件
    test_filename = "test_image.jpg"
    file_path = os.path.join(UPLOAD_DIRECTORY, test_filename)
    with open(file_path, "w") as f:
        f.write("old content")

    # 上傳新內容
    new_content = b"new content"
    file = BytesIO(new_content)

    response = client.put(f"/images/{test_filename}", files={"file": (test_filename, file, "image/jpeg")})
    assert response.status_code == 200
    assert response.json() == {"filename": test_filename}

    # 驗證文件內容已更新
    with open(file_path, "rb") as f:
        assert f.read() == new_content

def test_update_image_not_found():
    test_filename = "non_existent_image.jpg"
    new_content = b"new content"
    file = BytesIO(new_content)

    response = client.put(f"/images/{test_filename}", files={"file": (test_filename, file, "image/jpeg")})
    assert response.status_code == 404
    assert response.json() == {"detail": "Image not found"}

def test_update_image_internal_error(monkeypatch):
    test_filename = "test_image.jpg"
    file_path = os.path.join(UPLOAD_DIRECTORY, test_filename)
    with open(file_path, "w") as f:
        f.write("old content")

    new_content = b"new content"
    file = BytesIO(new_content)

    # 模擬寫入文件時出現異常
    def mock_open(*args, **kwargs):
        raise Exception("Unexpected error")
    
    monkeypatch.setattr("builtins.open", mock_open)

    response = client.put(f"/images/{test_filename}", files={"file": (test_filename, file, "image/jpeg")})
    assert response.status_code == 500
    assert response.json() == {"detail": "Internal Server Error"}

def test_delete_image_success():
    # 創建測試文件
    test_filename = "test_image.jpg"
    file_path = os.path.join(UPLOAD_DIRECTORY, test_filename)
    with open(file_path, "w") as f:
        f.write("test content")

    # 刪除文件
    response = client.delete(f"/images/{test_filename}")
    assert response.status_code == 200
    assert response.json() == {"message": "Image deleted"}

    # 確認文件已被刪除
    assert not os.path.exists(file_path)

def test_delete_image_not_found():
    response = client.delete("/images/non_existent_image.jpg")
    assert response.status_code == 404
    assert response.json() == {"detail": "Image not found"}

def test_delete_image_internal_error(monkeypatch):
    test_filename = "test_image.jpg"
    file_path = os.path.join(UPLOAD_DIRECTORY, test_filename)
    with open(file_path, "w") as f:
        f.write("test content")

    # 模擬刪除文件時出現異常
    def mock_os_remove(path):
        raise Exception("Unexpected error")

    monkeypatch.setattr(os, "remove", mock_os_remove)

    response = client.delete(f"/images/{test_filename}")
    assert response.status_code == 500
    assert response.json() == {"detail": "Internal Server Error"}
"""
def test_uvicorn_run(monkeypatch):
    def mock_run(app, host, port):
        assert host == "0.0.0.0"
        assert port == 8000

    monkeypatch.setattr("uvicorn.run", mock_run)

    with patch("main.__name__", "__main__"):
        import main
        assert main

def test_main_execution():
    # 檢查是否在 Windows 平台，調整虛擬環境的 Python 解釋器路徑
    #if sys.platform == 'win32':
    venv_python = os.path.join(os.getcwd(), 'env', 'Scripts', 'python.exe')
    
    # 執行 main.py 使用虛擬環境的 Python 解釋器
    result = subprocess.run([venv_python, "main.py"], capture_output=True, text=True)
    
    #result = subprocess.run(["python", "main.py"], capture_output=True, text=True)
    print("STDOUT:", result.stdout)
    print("STDERR:", result.stderr)
    assert result.returncode == 0
    assert "Uvicorn running on http://0.0.0.0:8000" in result.stderr
#
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)

def test_uvicorn_run(monkeypatch):
    
    #測試 uvicorn 的運行
    
    def mock_run(app, host, port):
        assert host == "0.0.0.0"
        assert port == 8000

    monkeypatch.setattr("uvicorn.run", mock_run)

    with patch("main.__name__", "__main__"):
        import main
        assert main

def test_main_execution(monkeypatch):
    
    #測試 main.py 的執行
    
    mock_subprocess_run = MagicMock(return_value=MagicMock(returncode=0, stderr="Uvicorn running on http://0.0.0.0:8000"))
    monkeypatch.setattr(subprocess, "run", mock_subprocess_run)

    result = subprocess.run(["python", "main.py"], capture_output=True, text=True)
    print("STDOUT:", result.stdout)
    print("STDERR:", result.stderr)
    assert result.returncode == 0
    assert "Uvicorn running on http://0.0.0.0:8000" in result.stderr
"""
