from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import FileResponse
from typing import List
import os
import logging

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}

# 設置日誌
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# 確保有一個存放圖片的目錄
UPLOAD_DIRECTORY = "./uploaded_images"
if not os.path.exists(UPLOAD_DIRECTORY):
    os.makedirs(UPLOAD_DIRECTORY)

# 上傳圖片
@app.post("/upload/", response_model=dict)
async def upload_image(file: UploadFile = File(...)):
    try:
        file_location = f"{UPLOAD_DIRECTORY}/{file.filename}"
        with open(file_location, "wb") as f:
            f.write(file.file.read())
        return {"filename": file.filename}
    except Exception as e:
        logger.error(f"Error uploading file: {e}")
        raise HTTPException(status_code=500, detail="Internal Server Error")

# 獲取圖片列表
@app.get("/images/", response_model=List[str])
async def list_images():
    try:
        files = os.listdir(UPLOAD_DIRECTORY)
        return files
    except Exception as e:
        logger.error(f"Error listing images: {e}")
        raise HTTPException(status_code=500, detail="Internal Server Error")

# 獲取單個圖片
@app.get("/images/{image_name}", response_class=FileResponse)
async def get_image(image_name: str):
    try:
        file_location = f"{UPLOAD_DIRECTORY}/{image_name}"
        if not os.path.exists(file_location):
            raise HTTPException(status_code=404, detail="Image not found")
        return file_location
    except HTTPException as e:
        raise e
    except Exception as e:
        logger.error(f"Error getting image: {e}")
        raise HTTPException(status_code=500, detail="Internal Server Error")

# 更新圖片
@app.put("/images/{image_name}", response_model=dict)
async def update_image(image_name: str, file: UploadFile = File(...)):
    try:
        file_location = f"{UPLOAD_DIRECTORY}/{image_name}"
        if not os.path.exists(file_location):
            raise HTTPException(status_code=404, detail="Image not found")
        
        with open(file_location, "wb") as f:
            f.write(file.file.read())
        return {"filename": image_name}
    except HTTPException as e:
        raise e
    except Exception as e:
        logger.error(f"Error updating image: {e}")
        raise HTTPException(status_code=500, detail="Internal Server Error")

# 刪除圖片
@app.delete("/images/{image_name}", response_model=dict)
async def delete_image(image_name: str):
    try:
        file_location = f"{UPLOAD_DIRECTORY}/{image_name}"
        if not os.path.exists(file_location):
            raise HTTPException(status_code=404, detail="Image not found")
        
        os.remove(file_location)
        return {"message": "Image deleted"}
    except HTTPException as e:
        raise e
    except Exception as e:
        logger.error(f"Error deleting image: {e}")
        raise HTTPException(status_code=500, detail="Internal Server Error")

# 啟動服務
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
