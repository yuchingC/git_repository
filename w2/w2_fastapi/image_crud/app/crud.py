import os
from fastapi import UploadFile, HTTPException
from fastapi.responses import FileResponse
from typing import Dict

# 定義圖片文件夾
IMAGE_DIR = "images"

# 創建圖片
async def create_image(file: UploadFile) -> Dict[str, str]:
    file_location = os.path.join(IMAGE_DIR, file.filename)
    
    if os.path.exists(file_location):
        raise HTTPException(status_code=400, detail="File already exists")

    with open(file_location, "wb") as f:
        f.write(await file.read())
    
    return {"filename": file.filename}

# 獲取圖片
def read_image(filename: str) -> FileResponse:
    file_location = os.path.join(IMAGE_DIR, filename)
    
    if not os.path.exists(file_location):
        raise HTTPException(status_code=404, detail="File not found")
    
    return FileResponse(file_location)

# 更新圖片
async def update_image(filename: str, file: UploadFile) -> Dict[str, str]:
    file_location = os.path.join(IMAGE_DIR, filename)
    
    if not os.path.exists(file_location):
        raise HTTPException(status_code=404, detail="File not found")
    
    with open(file_location, "wb") as f:
        f.write(await file.read())
    
    return {"filename": filename}

# 刪除圖片
def delete_image(filename: str) -> Dict[str, str]:
    file_location = os.path.join(IMAGE_DIR, filename)
    
    if not os.path.exists(file_location):
        raise HTTPException(status_code=404, detail="File not found")
    
    os.remove(file_location)
    
    return {"detail": "File deleted"}