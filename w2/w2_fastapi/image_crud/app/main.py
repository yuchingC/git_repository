from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
import os
import logging

app = FastAPI()
IMAGE_DIR = "images"
# 設置靜態文件夾來提供圖片
app.mount("/images", StaticFiles(directory="images"), name="images")

# 創建圖片文件夾如果不存在
if not os.path.exists("images"):
    os.makedirs("images")

@app.post("/upload/")
async def upload_image(file: UploadFile = File(...)):
    file_location = os.path.join(IMAGE_DIR, file.filename)
    
    if os.path.exists(file_location):
        raise HTTPException(status_code=400, detail="File already exists")

    with open(file_location, "wb") as f:
        f.write(await file.read())
    
    return {"filename": file.filename}

@app.get("/images/{filename}")
async def read_image(filename: str):
    file_location = os.path.join(IMAGE_DIR, filename)
    
    if not os.path.exists(file_location):
        raise HTTPException(status_code=404, detail="File not found")
    
    return FileResponse(file_location)

@app.put("/upload/{filename}")
async def update_image(filename: str, file: UploadFile = File(...)):
    file_location = os.path.join(IMAGE_DIR, filename)
    
    if not os.path.exists(file_location):
        raise HTTPException(status_code=404, detail="File not found")
    
    with open(file_location, "wb") as f:
        f.write(await file.read())
    
    return {"filename": filename}
'''
@app.put("/upload/{filename}", response_model=dict)
async def update_image(filename: str, file: UploadFile = File(...)):
    file_location = os.path.join(IMAGE_DIR, filename)

    if not os.path.exists(file_location):
        raise HTTPException(status_code=404, detail="File not found")

    try:
        with open(file_location, "wb") as f:
            f.write(file.file.read())

        return {"filename": filename}
    except Exception as e:
        logger.error(f"Error updating image: {e}")
        raise HTTPException(status_code=500, detail="Internal Server Error")
@app.put("/images/{image_name}", response_model=dict)
async def update_image(image_name: str, file: UploadFile = File(...)):
    try:
        file_location = f"{UPLOAD_DIRECTORY}/{image_name}"
        if not os.path.exists(file_location):
            raise HTTPException(status_code=404, detail="Image not found")
        
        with open(file_location, "wb") as f:
            f.write(file.file.read())
        return {"filename": image_name}
    except HTTPException as e:
        raise e
    except Exception as e:
        logger.error(f"Error updating image: {e}")
        raise HTTPException(status_code=500, detail="Internal Server Error")
'''
@app.delete("/upload/{filename}")
async def delete_image(filename: str):
    file_location = os.path.join(IMAGE_DIR, filename)
    
    if not os.path.exists(file_location):
        raise HTTPException(status_code=404, detail="File not found")
    
    os.remove(file_location)
    
    return {"detail": "File deleted"}
