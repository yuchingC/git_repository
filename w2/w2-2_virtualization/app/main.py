from fastapi import FastAPI, HTTPException
from databases import Database
import sqlalchemy

DATABASE_URL = "postgresql://postgres_user:postgres_pwd@db/postgres"

database = Database(DATABASE_URL)
metadata = sqlalchemy.MetaData()

notes = sqlalchemy.Table(
    "notes",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("text", sqlalchemy.String, nullable=False),
)

engine = sqlalchemy.create_engine(DATABASE_URL)
metadata.create_all(engine)

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.on_event("startup")
async def startup():
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

@app.post("/notes/")
async def create_note(text: str):
    query = notes.insert().values(text=text)
    last_record_id = await database.execute(query)
    return {"id": last_record_id, "text": text}

@app.get("/notes/")
async def read_notes():
    query = notes.select()
    return await database.fetch_all(query)
