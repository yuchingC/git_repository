import docker

# 創建Docker客戶端
client = docker.from_env()

# 拉取Hello World映像
print("Pulling hello-world image...")
client.images.pull('hello-world')

# 運行Hello World容器
print("Running hello-world container...")
container = client.containers.run('hello-world', detach=True)

# 查看容器輸出
print("Container logs:")
print(container.logs().decode('utf-8'))

# 停止並刪除容器
print("Stopping and removing the container...")
container.stop()
container.remove()

print("Done!")