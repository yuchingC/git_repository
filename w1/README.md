# 【AI SW_W1-2】git與python基本操作學習紀錄
本文件為2024/06/13學習git與python的紀錄

## 目錄
[TOC]

## 學習心得
經過這一週的學習，我對軟體工程師的職責、Git版本控制、Python的Pandas、requests和錯誤處理有了基本的了解。
### 本周學習時遇到的挑戰
1. git在合併分支時發現無法合併
    - 因為在遠端已經合併過一次同名的分支，但並沒有在本機刪除此分支，造成再度上傳資料後無法在遠端合併分支。
    - 最後在備份此分支下的資料後重新創建新的不同名分支去合併。
2. 在上傳.ipynb檔案時無法上傳
    - 原因是因為不小心把.ipynb加入.gitignore裡，修改後再度上傳成功。
### 本周的學習收穫

1. 認識軟體工程師的種類與工作內容

    在W1-1業師講解關於軟體工程師的不同類別及其工作內容。以下是我最感興趣的軟體工程師種類：

    - 大數據分析工程師 (Big Data Engineer)
    - 全端工程師 (Full Stack Developer)
    - 韌體工程師 (Firmware Engineer)

2. git版本控制

    W1-2學習Git版本控制工具：
    - 基本操作：
        - 克隆：通過git clone將repository克隆到本機。
        - 初始化：使用git init來初始化repository。
        - 新增檔案 ：使用echo新增文件
        - 添加文件到暫存區：使用git add將文件添加到暫存區，準備提交。
        - 提交更改：使用git commit提交更改，並用-m撰寫描述這次更改。
        - 分支管理：
            - 創建和切換分支：使用git branch創建新分支，並使用git checkout切換分支。
            - 合併分支：通過git merge將不同分支合併到主分支。
        - 遠程操作：
            - 推送和拉取：使用git push將本機更改推送到遠程repository，以及使用git pull從遠程拉取最新的更改到本機。
        - 衝突解決：
            - 解決合併衝突：在合併不同分支時，遇到衝突並解決。
        - 檢查狀態與歷史紀錄：
            - 檢查狀態：使用git status檢查repository的狀態
            - 查看歷史紀錄：使用git log查看過往版本紀錄，用--oneline查看簡化的紀錄
        - 創建.gitignore忽略清單

3. Python
    本週還學習Python的Pandas、requests以及錯誤處理。

    - Pandas：
        - 資料處理和分析：使用Pandas中的DataFrame來進行資料的讀取、處理和分析。
    - requests庫：
        - HTTP請求：使用requests庫發送GET和POST請求。
    - 錯誤處理：
        - 基礎異常處理：使用try-except來捕捉和處理異常。
        - 進階異常處理：使用else和finally塊來管理異常處理過程中的其他操作，確保資源能夠正確釋放和清理。


## git
### git安裝與確認
```bash!
#依照作業系統進行git安裝

git --version
#查看git版本，確認是否正確安裝
```

### git用戶資訊設置
```bash!
#git config 
#git資訊(設定使用者名稱與email)
git config user.name "使用者名稱"
git config user.email "使用者email@email.com"

#如果需要為每個repository設定同樣的使用者名稱和email，須加上'--global'
git config --global user.name "使用者名稱"
git config --global user.email "使用者email@email.com"
```

### 建立gitlab帳戶&建立新專案
- 註冊[gitlab](https://gitlab.com/)帳戶 / 登入現有帳戶
    - 註冊帳戶時，需使用[git用戶資訊設置](#git用戶資訊設置)設定的email
- 在gitlab上建立新專案
    ![image](https://hackmd.io/_uploads/SJ6iiHuHC.png)

### 設置SSH
```bash!
#建立SSH金鑰
ssh-keygen -t rsa -b 4096 -C "使用者email@email.com"
#-t rsa : 指定生成RSA類型的金鑰
#-b 4096 : 指定金鑰的長度為4096位
#-C "" : 添加註釋，通常為使用者email

#複製SSH金鑰
clip < ~/.ssh/id_rsa.pub
#金鑰位置預設為/c/Users/本機使用者名稱/.ssh/id_rsa.pub
```
- 在gitlab貼上金鑰
    - 使用者頭像 > 編輯個人資料 > SSH金鑰 > 新增新的金鑰
        ![image](https://hackmd.io/_uploads/S13GQLuSA.png)

### 建立專案(本機→gitlab) / 克隆專案(gitlab→本機) 
```bash!
#本機建立新專案並上傳至gitlab(本機→gitlab) 
mkdir [專案名稱]
#建立新專案資料夾

cd [專案名稱]
#前往專案所在資料夾

git init
#Initialize git，在使用全新的git專案時使用

git remote add origin git@gitlab.com:[使用者名稱]/[專案名稱].git
#新增遠端repository

git push -u origin main
#本機上傳gitlab
```
```bash!
#複製已有的gitlab專案，下載到本機(gitlab→本機) 
git clone git@gitlab.com:[使用者名稱]/[專案名稱].git
#將gitlab上的專案克隆到本機
``` 

### 專案內文件操作
```bash!
echo "文件內文字" > README.md #文件名稱.副檔名
#新增文件並寫入內容，如果文件已經存在，則覆蓋內容

echo "新增文件文字" >> README.md
#追加文件內容，將'新增文件文字'加入到README.md的文件末端，且不覆蓋原本內容
```

### 添加文件到暫存區
```bash!
git add [文件名稱].[副檔名]
#添加指定文件

git add [A文件名稱].[副檔名] [B文件名稱].[副檔名]
#添加多個指定文件

git add .
#添加所有文件

git add [資料夾名稱]/
#添加指定資料夾以及其子資料夾的所有文件到暫存區
```

### 提交更改
```bash!
git commit -m "更改內容的註釋"
#註釋內容例如 : update file content、add README.md，清楚描述更新事項

##修改最近的一項註釋
git commit --amend -m "新的提交描述"
```

### git branch基本操作
```bash!
git branch
#查看本機所有分支，分支前有'*'為當前所在分支
git branch -r
#查看遠端所有分支
git branch -a
#查看本機&遠端所有分支

git branch [分支名稱]
#新建一個名稱為[分支名稱]的分支

git branch -d [分支名稱]
#刪除本機分支

git checkout [分支名稱]
#切換分支
git checkout -b [分支名稱]
#新建分支並切換到新分支
```
- 建立分支時不要重複建立已經合併過的分支名稱，不然會造成再次合併時出現問題。

### 本機合併分支 / 合併時發生衝突
```bash!
#main為主分支

git checkout main
#切換到主分支
git merge [要合併的分支名稱]
#將[要合併的分支名稱]合併到當前所在分支
```
- 當合併時發生衝突時，顯示以下訊息 : 
![image](https://hackmd.io/_uploads/rysZbKdH0.png)
![image](https://hackmd.io/_uploads/r19_WKOBR.png)
![image](https://hackmd.io/_uploads/Hk75WKuHR.png)
- 修改完衝突檔案後，將檔案加入暫存區，並提交更改 : 
```bash!
git add .
git commit -m "fix conflict file"
```
- 再次查看狀態會顯示以下訊息，表示衝突解決 : 
![image](https://hackmd.io/_uploads/Skpgmt_BA.png)

### gitlab合併分支
![image](https://hackmd.io/_uploads/ByEGAqOrR.png)



### 本機與遠端(gitlab)的操作
```bash!
git push origin [branch名稱]
#本機→gitlab

git pull origin [branch名稱]
#gitlab→本機，結合git fetch和git merge，獲取遠端最新的更動並合併
git fetch origin
#gitlab→本機，獲取遠端最新的更動，但不會合併更改
git fetch -p
#當gitlab與本機使用'git branch -a'命令看到的分支列表不一致時使用
```

### 檢查狀態&歷史紀錄
```bash!
git status
#檢查repository的狀態

git log
#查看歷史紀錄
git log --oneline
#查看簡化的歷史紀錄(顯示為一行)
```

### 忽略清單(.gitignore)
- 以python為主的忽略清單，目的為排除不需要加入git版控的檔案
```csharp!
#Python
*.pyc
*.pyo
*.pyd
__pycache__/

#Jupyter Notebooks
.ipynb_checkpoints
#Jupyter Notebook outputs
*.ipynb

#Environments
.env
.venv
ENV/
env/
venv/

#Logs
*.log

#Virtual environment
.vscode/

#PyCharm
.idea/
```
- 但在專案開始後才加入.gitignore是無法直接取消追蹤的，已經被追蹤的檔案都會持續被記錄
```bash!
git rm -f [檔案名稱]
#從版本控制中移除[檔案名稱]，並從工作目錄中刪除它
git rm -rf --cached [資料夾名稱]
#從版本控制中移除[資料夾名稱]及其所有內容，但保留工作目錄中的[資料夾名稱]及其內容。

git clean -fX
#刪除所有被.gitignore忽略的文件，f代表file 
git clean -fd  
#刪除所有未追蹤的文件和目錄，d代表directory
```
---
## python
以下內容在[[AI SW] w1-2.ipynb]([AI_SW]_w1_2.ipynb)有更詳細的紀錄
### 設置環境
```python!
#安裝pandas、requests； 另外為了使用iris資料集，安裝sklearn
!pip install pandas requests scikit-learn
```
### 利用Pandas做資料處理
#### iris資料集簡介
- iris是一個關於鳶尾花的資料集，包含4種屬性與3種標籤 :
  - 屬性為 :
    1. Sepal length: 花萼長度 (cm)
    2. Sepal width: 花萼寬度 (cm)
    3. Petal length: 花瓣長度 (cm)
    4. Petal width: 花瓣寬度 (cm)

  - 3種標籤為鳶尾花種類 :
    1. setosa: 山鳶尾
    2. versicolor: 變色鳶尾
    3. virginica: 維吉尼亞鳶尾
```python!
#從sklearn中導入load_iris函數
from sklearn.datasets import load_iris
#載入iris資料集
iris = load_iris()

#以下為iris資料集的結構與資料
#print key跟value.shape
print("\033[34mprint key跟value.shape\033[0m")
for key,value in iris.items() :
  try:
    print (key,value.shape)
  except:
    print (key)

#print屬性名稱
print("\n\033[34mprint屬性名稱\033[0m")
print (iris['feature_names'])

#print資料集描述
print("\n\033[34mprint資料集描述\033[0m")
print (iris['DESCR'])
```
#### Pandas常用功能
```python!
#載入pandas
import pandas as pd

#將資料集轉換為pandas dataframe，並設置columns為特徵名稱。
df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
#顯示前五行資料
df.head()

#將目標標籤'species_label'添加到dataFrame中
df['species_label'] = iris.target
#顯示前五行數據
df.head()

#讓品種對應編號
species_data = {
    'species_label': [0, 1, 2],
    'species': ['Setosa', 'Versicolor', 'Virginica']
    }
#並建立內含品種與標籤的dataframe "df_z"
df_z = pd.DataFrame(species_data, columns = ['species_label', 'species'])

#檢視df_z
df_z.head()

#基於'species_label'列，合併df和df_z，讓新的'df'包含每個樣本的品種名稱
df=pd.merge(df, df_z, on='species_label')
#觀察前五個觀測值
df.head()

#查看iris的基本資料
df.info()

#描述資料集的統計資訊
df.describe()

#按標籤類別計數
df['species'].value_counts()

#只觀察標籤類別為Versicolor的資料
Versicolor = df[df['species'] == 'Versicolor']
Versicolor.head()

#觀察花瓣長度(petal width (cm))大於5cm的Versicolor
Versicolor[Versicolor['petal length (cm)'] > 5]

#對花瓣長度由小到大排列
Versicolor=Versicolor.sort_values(by=['petal length (cm)'])
Versicolor.head(10)
```

### 使用Requests發送HTTP請求
```python!
import requests

#基礎URL
base_url = 'https://jsonplaceholder.typicode.com/posts'

#1.GET請求，查詢參數
response_get = requests.get(base_url, params={'userId': 1})
print("GET Response:")
print(response_get.status_code, " | 檢查請求的狀態碼，出現200代表請求成功")
print(response_get.json(), " | 解析並顯示JSON響應")

#2.POST請求，用於創建資源，可以包含表單數據或JSON數據
data_post = {'title': 'foo', 'body': 'bar', 'userId': 1}#要發送的資料
response_post = requests.post(base_url, json=data_post)#json是要發送的JSON資料
print("\nPOST Response:")
print(response_post.status_code, " | 201表示資源創建成功")
print(response_post.json(), " | 解析並顯示JSON格式的響應內容")

#3.PUT請求，更新整個資源
data_put = {'id': 1, 'title': 'foo', 'body': 'bar', 'userId': 1}#要更新的資料
response_put = requests.put(base_url + '/1', json=data_put)
print("\nPUT Response:")
print(response_put.status_code, " | 200表示成功")
print(response_put.json(), " | 解析並顯示JSON響應")

#4.PATCH請求，更新部分資源
data_patch = {'title': 'updated title'}#要更新的資料
response_patch = requests.patch(base_url + '/1', json=data_patch)
print("\nPATCH Response:")
print(response_patch.status_code, " | 200表示請求成功")
print(response_patch.json(), " | 解析並顯示JSON響應")

#5.DELETE請求，用於刪除資料
response_delete = requests.delete(base_url + '/1')
print("\nDELETE Response:")
print(response_delete.status_code, " | 出現200或204代表刪除成功")
print(response_delete.text, " | 應該會出現空欄")

#6.HEAD請求，只獲取響應頭資訊，例如內容類型或內容長度
response_head = requests.head(base_url + '/1')
print("\nHEAD Response:")
print(response_head.status_code, " | 200表示請求成功")
print(response_head.headers, " | 顯示響應頭資訊")

#7.通用請求方法
response_request = requests.request('GET', base_url, params={'userId': 1})#'GET'的位置放的是HTTP方法，除了GET以外，有POST、PUT等上面有提到的方法；params是查詢參數
print("\nGeneric Request Response:")
print(response_request.status_code, " | 200表示請求成功")
print(response_request.json(), " | 解析並顯示JSON響應")
```
### 錯誤處理
```python!
#除0錯誤
result = 10 / 0
print(result)
```
![image](https://hackmd.io/_uploads/Hk6kIhprC.png)
```python!
#基本錯誤處理
try:
    result = 10 / 0
except ZeroDivisionError as e:
    print(f"Error occurred: {e}")
    
#處理多種異常
try:
    result = 10 / 0
except ZeroDivisionError as e:
    print(f"Division error: {e}")
except Exception as e:
    print(f"An error occurred: {e}")
    
#使用else和finally
try:
    result = 10 / 2
except ZeroDivisionError as e:
    print(f"Division error: {e}")
else:
    print(f"Result is: {result}")
finally:
    print("This will always be executed\n")

try:
    result = 10 / 0
except ZeroDivisionError as e:
    print(f"Division error: {e}")
else:
    print(f"Result is: {result}")
finally:
    print("This will always be executed")
```